# Atmosphe.RED UCL Master Project

Atmosphe.RED is a library avaliable to Unity and Unreal games to enhance gamer experiences. The library was created to connect any game to Internet of Things (IoT) devices including Amazon's Echo (Alexa) and Philips Hue. To develop the library the team used; Node-RED, IBM's IoT Platform, Unity, Unreal, Amazon Echo (Gen 2), Philips Hue (with colour), and Raspberry Pi's. This project is under the licence agpl v3.

<!-- ## Unit Testing
	- Run this command in your Raspberry Pi's ~/.node-red directory to execute a unit test:
		- node node_modules/mocha/bin/mocha test/AlexaTestFlow/alexa-test_spec.js -->